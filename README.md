# Recyclearn

## Rodar o projeto
```
npm install
```

### Rodar localmente
```
npm run serve
```

### Compilar modo produção
```
npm run build
```

### Equipe

- **Ana Carolina Teodoro**
    - [Email](teodoro.c.aninha@gmail.com)
    - [Linkedin](https://www.linkedin.com/in/ana-carolina-teodoro-188309234)

- **Bruno Rekssua Pelentir**
    - [E-mail](brunopelentir02@gmail.com)
    - [Lindekin](https://www.linkedin.com/in/bruno-rekssua-pelentir)

- **Deógenes Nicoletti**
    - [E-mail](dogeness@gmail.com)
    - [Linkedin](https://www.linkedin.com/in/de%C3%B3genes-nicoletti)

- **Joiana Horrana dos Santos Queiroz**
    - [E-mail](joianaqueirozz@gmail.com)
    - [Linkedin](https://www.linkedin.com/in/joiana-horrana-dos-santos-queiroz-1aaa771b6/)

### Links úteis

- [Aplicação PWA](http://deogenes.vps-kinghost.net/)
- [Design System](.readme/Design_Sytem-Recyclearn_PDF.pdf)
- Figma: Telas de alta e baixa fidelidade
    - [PDF](.readme/Recyclearn.pdf)
    - [.fig](.readme/Recyclearn.fig)
    - [Site](https://www.figma.com/file/7PHTnqPB46143BhXBWfddN/Recyclearn?type=design&node-id=0%3A1&mode=design&t=LyMrs8g2ebyu1IwP-1)
    - [Play mode](https://www.figma.com/proto/7PHTnqPB46143BhXBWfddN/Recyclearn?type=design&node-id=0-1&scaling=scale-down&page-id=0%3A1)