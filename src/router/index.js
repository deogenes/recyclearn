import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import ColetaClienteView from '../views/ColetaClienteView.vue'

const routes = [
  {
    path: '/',
    name: 'Default',
    component: function () {
      return import(/* webpackChunkName: "about" */ '../views/LoginView.vue')
    }
  },
  {
    path: '/user-profile',
    name: 'Profile',
    component: function () {
      return import(/* webpackChunkName: "about" */ '../views/UserProfile.vue')
    }
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: HomeView
  },
  {
    path: '/login',
    name: 'Login',
    component: function () {
      return import(/* webpackChunkName: "about" */ '../views/LoginView.vue')
    }
  },
  {
    path: '/register',
    name: 'Cadastro',
    component: function () {
      return import(/* webpackChunkName: "about" */ '../views/CadastroView.vue')
    }
  },
  {
    path: '/venda-materiais',
    name: 'VendaMateriais',
    component: function () {
      return import(/* webpackChunkName: "about" */ '../views/VendaMateriaisView.vue')
    }
  },
  {
    path: '/coleta-cliente',
    name: 'Coleta Cliente',
    component: ColetaClienteView
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
