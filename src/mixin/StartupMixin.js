var StartupMixin = {
    mounted(){
        let sources = [
            "/js/vendor.min.js",
            "/js/app.min.js"
        ];
    
        for(let source of sources)
        {
            let script = document.createElement('script');
            script.setAttribute("src", source);
    
            //document.querySelector('#app').insertAdjacentElement("afterend", script)
        }
    }
}

export default StartupMixin;